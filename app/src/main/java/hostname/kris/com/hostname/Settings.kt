package hostname.kris.com.hostname

import android.os.Bundle
import android.preference.PreferenceActivity
import android.preference.PreferenceManager

class Settings : PreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onPause() {
        super.onPause()

        val sp = PreferenceManager.getDefaultSharedPreferences(this)

        if (sp.getBoolean("enabled", false) && !sp.getString("hostname", "").isEmpty()){
            setHostname(sp.getString("hostname", ""));
        }

    }
}
package hostname.kris.com.hostname

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager


class OnBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            val sp = PreferenceManager.getDefaultSharedPreferences(context)

            if (sp.getBoolean("enabled", false) && !sp.getString("hostname", "").isEmpty()){
                setHostname(sp.getString("hostname", ""));
            }
        }
    }
}

fun setHostname(hostname: String) {
    try {
        Runtime.getRuntime().exec("setprop net.hostname " + hostname);
    } catch (e: Exception) { }
}

